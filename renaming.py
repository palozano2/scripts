# Python3 code to rename multiple files
# in a directory or folder

import os

def main():

    for filename in os.listdir('.'):

        if filename[:10]=='Screenshot':
            src = filename
            dst = 'Captura de pantalla ' + filename[11:21] + ' a las ' + filename[22:] 
            os.rename(src, dst)

if __name__ == '__main__':
    main()
