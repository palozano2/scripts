#!/usr/bin/env python
from plistlib import readPlistFromString as rPFS
from subprocess import *

def shell(cmd):
    return Popen(cmd.split(), stdout=PIPE).communicate()[0]

disks = {False: [], True: []}   
for disk in rPFS(shell('diskutil list -plist'))['WholeDisks']:
    disks[rPFS(shell('diskutil info -plist ' + disk))['Internal']].append(disk)

print("Internal disks: " + ' '.join(disks[True]) )   
print("External disks: " + ' '.join(disks[False]) ) 
