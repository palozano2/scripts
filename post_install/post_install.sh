#!/bin/sh
# -*- Mode: sh; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
#
# Authors:
#   pablo l 
#
# Description:
#   A post-installation bash script for Ubuntu
#######################################################################

# add repositories
# regolith ppa repository
sudo add-apt-repository ppa:kgilmer/regolith-stable

# basic update
sudo apt-get -y update
#sudo apt-get -y --force-yes update
sudo apt-get -y upgrade
#sudo apt-get -y --force-yes upgrade


# instal apps
sudo apt-get install \
	sublime-text-installer git vim \
	unity-tweak-tool curl texlive-full \
	texstudio regolith-desktop r-base \
    libopenblas-base valgrind

# python3, neovim, filezille

# install Dropbox
#cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
#~/.dropbox-dist/dropboxd

# install r-studio
cd ~/Downloads
wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.5019-amd64.deb
sudo dpkg -i rstudio-xenial-1.1.379-amd64.deb


# remove some folders
rm -rf ~/Public
rm -rf ~/Templates
rm -rf ~/Videos
rm -rf ~/Music
rm ~/examples.desktop


# prompt for a reboot
clear
echo ""
echo "===================="
echo " TIME FOR A REBOOT! "
echo "===================="
echo ""

# make it executable
# sudo chmod +x Ubuntu_post_installation.sh

# run as root
# sudo ./post_install.sh
