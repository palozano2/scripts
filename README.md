# scripts
Scripts for automating certain tasks


## combinePDFs.py
Combines all the PDFs in the current folder, in alphabetical order.


## list-fftabs.py
Lists all the tabs from the Firefox browser that is running.


## scrapPDFs.py
Scraps the info from a PDF file to extract info. Meant to work for categorizing tickets.

## renaming.py
Batch renaming for files in a folder.

## rsync\_python.sh and rsync\_udev\_trigger.sh
Rsync using Python (work in progress)

## post\_install
Update the system, install certain programs, configure everything the way I like, and more.

## makefiles
Make for different purposes: LaTeX, C programming, Python programming, etc.
