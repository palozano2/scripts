import PyPDF2
import textract

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


filename = 'ticketprueba' 
pdfFileObj = open(filename,'rb')

pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
num_pages = pdfReader.numPages

count = 0
text = ""

#The while loop will read each page
while count < num_pages:
    pageObj = pdfReader.getPage(count)
    count +=1
    text += pageObj.extractText()

#This if statement exists to check if the above library returned #words. It's done because PyPDF2 cannot read scanned files. If the below returns as False, we run the OCR library textract to #convert scanned/image based PDF files into text

if text != "":
   text = text
else:
   text = textract.process(fileurl, method='tesseract', language='eng')

print(text)
