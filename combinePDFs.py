#! python3
# combinePDFs.py - Combines all the PDFs in the current working directory into
# into a single PDF.

import PyPDF2, os
from PyPDF2 import PdfFileMerger

pagina_comienzo = 0 # 0 no quita ninguna pagina del principio

# Get all the PDF filenames.
pdfFiles = []
for filename in os.listdir('.'):
    if filename.endswith('.pdf'):
        pdfFiles.append(filename)
pdfFiles = sorted(pdfFiles, key=str.lower)

pdfWriter = PyPDF2.PdfFileWriter()

# Loop through all the PDF files.
for filename in pdfFiles:
    pdfFileObj = open(filename, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj, strict=False)
    # Loop through all the pages (except ...) and add them.
    for pageNum in range(pagina_comienzo, pdfReader.numPages):
        pageObj = pdfReader.getPage(pageNum)
        pdfWriter.addPage(pageObj)

# Save the resulting PDF to a file.
pdfOutput = open('PDFconjunto.pdf', 'wb')
pdfWriter.write(pdfOutput)
pdfOutput.close()